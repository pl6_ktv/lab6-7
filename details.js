const product = JSON.parse(localStorage.getItem('detalno'));
		const nameProduct = document.querySelector('.nazva');
		nameProduct.innerHTML = product ? product.name : '';

		const priceProduct = document.querySelector('.cina');
		priceProduct.innerHTML = product ? product.price : '';

		// Відновлення коментарів при завантаженні сторінки
		const commentsContainer = document.querySelector('.comments');
		const pageId = product ? product.id : 'default'; // Ідентифікатор сторінки
		const savedComments = JSON.parse(localStorage.getItem(`comments_${pageId}`)) || [];
		savedComments.forEach(savedComment => {
			commentsContainer.appendChild(createCommentElement(savedComment));
		});

		function createCommentElement(commentData) {
			const comment = document.createElement('div');
			comment.classList.add('comment');

			const pName = document.createElement('p');
			pName.textContent = commentData.name;

			const pDate = document.createElement('p');
			pDate.textContent = commentData.date;

			const pText = document.createElement('p');
			pText.textContent = commentData.text;

			comment.appendChild(pName);
			comment.appendChild(pDate);
			comment.appendChild(pText);

			return comment;
		}

		function createForm() {
			let nameInput = document.querySelector('.formCreate input[type="text"]');
			let textArea = document.querySelector('.formCreate textarea');

			let name = nameInput.value;
			let text = textArea.value;

			// Створення об'єкта коментаря
			const currentDate = new Date();
			const commentData = {
				name: name,
				date: `${currentDate.getHours()}:${currentDate.getMinutes()}:${currentDate.getSeconds()}`,
				text: text
			};

			// Додавання коментаря до відображення на сторінці
			const commentElement = createCommentElement(commentData);
			commentsContainer.appendChild(commentElement);

			// Зберігання коментаря в localStorage
			const pageId = product ? product.id : 'default'; // Ідентифікатор сторінки
			const savedComments = JSON.parse(localStorage.getItem(`comments_${pageId}`)) || [];
			savedComments.push(commentData);
			localStorage.setItem(`comments_${pageId}`, JSON.stringify(savedComments));

			// Очищення полів вводу
			nameInput.value = "";
			textArea.value = "";
		}