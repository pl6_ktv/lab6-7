class Tovar {
	constructor(name, price, image) {
		this.id = Date.now();
			this.name = name;
			this.price = price;
			this.image = image || '/default-image.jpg';
	}
}

class tovarCRUD {
	constructor() {
			this.products = [] || JSON.parse(localStorage.getItem('tovaru'));
	}

	createProduct(tovar) {
			this.products.push(tovar);
			localStorage.setItem('tovaru', JSON.stringify(this.products))
	}
	showInfo() {
			const container = document.querySelector('.container');
			container.innerHTML = "";

			this.products = JSON.parse(localStorage.getItem('tovaru'));
			this.products.forEach((product) => {
					const tovar = document.createElement('div');
					tovar.classList.add('tovar');
					container.append(tovar);

					const tovarName = document.createElement('div');
					tovarName.classList.add('tovarName');
					tovarName.textContent = product.name;
					tovar.append(tovarName);

					const tovarPrice = document.createElement('div');
					tovarPrice.classList.add('tovarPrice');
					tovarPrice.textContent = product.price;
					tovar.append(tovarPrice);

					const tovarImage = document.createElement('img');
					tovarImage.classList.add('tovarImage');
					tovarImage.src = product.image;
					tovar.append(tovarImage);
				//  коментарі до кнокретного товару
					const detalno = document.createElement('a');
					detalno.href = "details.html";
					detalno.textContent = "Show more";
					tovar.append(detalno);
					detalno.addEventListener('click', ()=>{
						localStorage.setItem('detalno', JSON.stringify(product))
						window.location.href = "details.html";
					})
			});
	}
}

const tovaru = new tovarCRUD();

const form = document.querySelector('.form');
const inputName = form.children[0];
const inputPrice = form.children[1];
const inputImage = form.children[2];

if (JSON.parse(localStorage.getItem('tovaru')) != undefined) {
	tovaru.showInfo();
}

form.addEventListener('submit', (e) => {
	e.preventDefault();
	
	tovaru.createProduct(new Tovar(inputName.value, inputPrice.value, inputImage.value));
	tovaru.showInfo();
	inputName.value = '';
	inputPrice.value = '';
	inputImage.value = '';
});

// localStorage.removeItem('tovaru');

const searchForm = document.querySelector('.search-form');
const searchBox = document.getElementById('search-box');

searchForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const searchTerm = searchBox.value.toLowerCase();
    const filteredProducts = tovaru.products.filter(product =>
        product.name.toLowerCase().includes(searchTerm)
    );
    tovaru.showInfo(filteredProducts); 
});


const sortSelect = document.getElementById('sort-by');

sortSelect.addEventListener('change', () => {
    const sortBy = sortSelect.value;
    const sortedProducts = tovaru.products.slice().sort((a, b) => {
        if (sortBy === 'name') {
            return a.name.localeCompare(b.name);
        } else if (sortBy === 'price') {
            return a.price - b.price;
        }
    });
    displayProducts(sortedProducts);
});

// пошук товару
function searchProducts() {
	const searchTerm = searchBox.value.toLowerCase();
	const foundProduct = tovaru.products.find(product => product.name.toLowerCase() === searchTerm);

	if (foundProduct) {
			localStorage.setItem('detalno', JSON.stringify(foundProduct));
			window.location.href = "details.html";
	}

}

