const tovaru = JSON.parse(localStorage.getItem('tovaru')) || [];
const categories = JSON.parse(localStorage.getItem('categories')) || [];
const table = document.querySelector('.table');


// Функція для відображення товарів у таблиці
function showTable() {
	table.innerHTML = ''; // Очистити таблицю перед відображенням

	// Додати рядок для заголовків
	const headerRow = document.createElement('tr');
	const indexHeader = document.createElement('th');
	indexHeader.textContent = '№';
	headerRow.appendChild(indexHeader);

	// Додавання заголовків
	const relevantHeaders = ['Назва товару', 'Ціна', 'Редагувати', 'Видалити'];
	relevantHeaders.forEach(headerText => {
			const th = document.createElement('th');
			th.textContent = headerText;
			headerRow.appendChild(th);
	});

	table.appendChild(headerRow);

	tovaru.forEach((element, i) => {
			const tr = document.createElement('tr');
			table.append(tr);

			// Додавання індекса у форматі 1, 2, 3
			const indexTd = document.createElement('td');
			indexTd.textContent = i + 1;
			tr.append(indexTd);

			// виведення відповідних даних
			const relevantKeys = ['name', 'price'];
			relevantKeys.forEach(key => {
					const td = document.createElement('td');
					tr.append(td);
					td.textContent = element[key];
			});

			const edit = document.createElement('td');
			edit.innerHTML = "&#9998;";
			tr.append(edit);

			const removeElem = document.createElement('td');
			removeElem.innerHTML = "&#10060;";
			tr.append(removeElem);
			removeElem.addEventListener('click', () => {
					vudalutuTovar(i);
					showTable();
			});

			// Додавання події для редагування
			edit.addEventListener('click', () => {
					fillEditTable(i);
			});
	});
}


// Функція для заповнення таблиці редагування
function fillEditTable(index) {
    const editTable = document.getElementById('editTable');
    const tr = document.createElement('tr');
    editTable.innerHTML = ''; // Очистити попередні дані

    for (let key in tovaru[index]) {
        const td = document.createElement('td');
        tr.append(td);

        const input = document.createElement('input');
        input.type = 'text';
        input.value = tovaru[index][key];
        td.append(input);
    }
		

    const editButton = document.createElement('td');
    editButton.innerHTML = "&#128393;";
    tr.append(editButton);

    editButton.addEventListener('click', () => {
        saveChanges(index, tr);
    });

    editTable.append(tr);
}

// Функція для збереження змін
function saveChanges(index, editTableRow) {
    const inputs = editTableRow.querySelectorAll('input');

    for (let i = 0; i < inputs.length; i++) {
        const key = Object.keys(tovaru[index])[i];
        tovaru[index][key] = inputs[i].value;
    }

    localStorage.setItem('tovaru', JSON.stringify(tovaru));
    showTable();
}

// Функція для видалення товару
const vudalutuTovar = (index) => {
    tovaru.splice(index, 1);
    localStorage.setItem('tovaru', JSON.stringify(tovaru));
}

// Викликати функцію для відображення таблиці при завантаженні сторінки
showTable();

